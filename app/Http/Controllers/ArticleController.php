<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ArticleController extends Controller
{
    public function home(){
        return view('article.home');
    }

    public function show1(){
        $client = new Client();
        $url = 'https://gitlab.com/api/v4/users/ikhwanrnurzaman/projects';
        $response = $client->get($url);
        $project = json_decode($response->getBody()->getContents());
        // dd($project);
        return view('article.show1', compact('project'));
    }
    
    public function show2(){
        $client = new Client();
        $url = 'https://gitlab.com/api/v4/users/yudhi.riyansyah/projects';
        $response = $client->get($url);
        $project = json_decode($response->getBody()->getContents());
        // dd($project);
        return view('article.show2', compact('project'));
    }

    public function create(){
        return view('article.create');
    }

    public function store(Request $request){
        $name = $request->name;
        $desc = $request->desc;
        $client = new Client();
        $url = 'https://gitlab.com/api/v4/projects';
        $header = array('headers'=>['Content-Type'=>'application/x-www-form-urlencoded','PRIVATE-TOKEN'=>'qkrUhHvxGmWjEnsioZfn'],'form_params'=>['name'=>$name,'description'=>$desc, 'visibility'=>'public']);
        // $form_param = ['form_params'=>['name'=>$name,'description'=>$desc]];
        $response = $client->post($url, $header);

        return redirect('/ikhwanrnurzaman');
    }
}
