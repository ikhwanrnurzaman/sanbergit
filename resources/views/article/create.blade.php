@extends('layouts.home')

@section('post')
<form action="/store" method="POST">
    <input type="text" name="name" class="form-control m-1" placeholder="Project Name">
    <input type="text" name="desc" class="form-control m-1" placeholder="Description">
    <input type="submit" name="submit" value="Create" class="btn btn-primary">
    {{ csrf_field() }}
</form>
@endsection