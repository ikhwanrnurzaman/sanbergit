@extends('layouts.home')

@section('post')
<table class="table">
    <tr>
        <th>ID</th>
        <th>Project Name</th>
        <th>Web URL</th>
        <th>Star Count</th>
        <th>Last Activity</th>
        <th>Action</th>
    </tr>
    @foreach ($project as $item)
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->name}}</td>
            <td><a href="{{$item->web_url}}" target="_blank">{{$item->web_url}}</a></td>
            <td><span>&#9733;</span>{{$item->star_count}}</td>
            <td>{{$item->last_activity_at}}</td>
            <td><a href="" class="btn btn-primary">Star</a></td>
        </tr>
    @endforeach
</table>
<div class="row justify-content-center my-2">
    <a href="" class="btn btn-primary"> New Project </a>
</div>
@endsection