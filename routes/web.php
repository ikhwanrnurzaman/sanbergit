<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', 'ArticleController@home');

Route::get('/ikhwanrnurzaman', 'ArticleController@show1');

Route::get('/yudhiriyansyah', 'ArticleController@show2');

Route::get('/create','ArticleController@create');

Route::post('/store','ArticleController@store');
